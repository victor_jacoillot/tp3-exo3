﻿namespace TP2
{
    public class Squelette : Monstre
    {
        public Squelette() : base()
        {
            this.vie *= 1.8;
            this.pointAttaque *= 0.8;
        }
    }
}