﻿namespace TP2
{
    public class Gobelin : Monstre
    {
        public Gobelin() : base()
        {
            this.vie *= 0.8;
            this.pointAttaque *= 1.2;
        }
    }
}