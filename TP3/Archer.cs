﻿using System;

namespace TP2
{
    public class Archer : Hero
    {
        public Archer(Hero hero) : base(hero.GetVie() * 1.15, hero.GetAttaque()* 1.15)
        {
            Console.WriteLine("Hero -> Archer");
        }
    }
}