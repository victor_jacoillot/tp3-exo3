﻿namespace TP2
{
    public class Sorciere : Monstre
    {
        public Sorciere() : base()
        {
            this.vie *= 0.5;
            this.pointAttaque *= 1.5;
        }
    }
}