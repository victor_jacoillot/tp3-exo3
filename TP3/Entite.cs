﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2
{
    public abstract class Entite
    {
        protected String name;
        protected bool enVie;
        protected double vie;
        protected double pointAttaque;


        public Entite(double vie, double attaque)
        {
            this.name = GetType().Name;
            this.vie = Math.Abs(vie);
            this.pointAttaque = Math.Abs(attaque);
            this.enVie = true;

        }
        public void Attaque(Entite entite)
        {
            entite.PredreVie(this.pointAttaque);
            Console.WriteLine($"{this.name} (PV : {Math.Round(vie,0)} ) attaque {entite.name} !");
            Console.WriteLine($"{entite.name} a encore {Math.Round(entite.vie,0)} PV !");

        }

        public void PredreVie(double nbPointVie)
        {
            vie -= nbPointVie;
            if(vie <= 0)
            {
                enVie = false;
            }

        }

        public bool isEntiteVivante() { return this.enVie; }
    }
}
