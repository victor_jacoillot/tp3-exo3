﻿using System;

namespace TP2
{
    public class Chevalier : Hero
    {
        public Chevalier(Hero hero) : base(hero.GetVie() * 1.20, hero.GetAttaque() * 1.10)
        {
            Console.WriteLine("Hero -> Chevalier");
        }
    }
}