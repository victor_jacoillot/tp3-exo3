﻿using System;

namespace TP2
{
    public class Magicien : Hero
    {

        public Magicien(Hero hero) : base(hero.GetVie() * 1.10, hero.GetAttaque() * 1.20)
        {
            Console.WriteLine("Hero -> Magicien");
        }
    }
}