﻿using System;
using System.Linq;

namespace TP2
{
    class GestionnaireDePartie
    {
        static void Main(string[] args)
        {
            Exo3(new Hero(200,20));
        }

        static void Exo3(Hero hero)
        {
            Random monstreRandom = new Random();
            int monstreMort = 0;
            Monstre unMonstre;
            while (hero.isEntiteVivante())
            {
                switch (monstreRandom.Next(0, 4))
                {
                    case 1:
                        unMonstre = new Gobelin();
                        break;
                    case 2:
                        unMonstre = new Squelette();
                        break;
                    case 3:
                        unMonstre = new Sorciere();
                        break;
                    default:
                        unMonstre = new Gobelin();
                        break;
                }

                while (unMonstre.isEntiteVivante() && hero.isEntiteVivante())
                {
                    hero.Attaque(unMonstre);
                    Console.WriteLine();
                    if (unMonstre.isEntiteVivante())
                    {
                        unMonstre.Attaque(hero);
                        Console.WriteLine();
                    }
                    

                }
                monstreMort++;
                if (hero.isEntiteVivante())
                {
                    hero.UnTresor();
                }
                if (monstreMort == 5)
                {
                    int res = monstreRandom.Next(1,3);
                   if (res == 1)
                    {
                        hero = new Magicien(hero);
                    }
                    else if(res == 2) {
                        hero = new Archer(hero);
                    }
                    else if(res == 3)
                    {
                        hero = new Chevalier(hero);
                    }
                }
            }
            Console.WriteLine($"Défaite du Hero ! Les monstres ont eu {monstreMort} morts !");

        }
    }
}
