﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2
{
    public class Hero : Entite
    {
        private static Random random = new Random();

        public Hero(double vie, double attaque) : base(vie, attaque)
        {
            this.vie = random.Next((int)vie);
            this.pointAttaque = random.Next((int)attaque);
        }

        public void UnTresor()
        {
            if(random.Next(0,10) >= 5)
            {
                vie += 20;
                Console.WriteLine("\nDe la vie en plus !");
            }
            else
            {
                vie -= 5;
                Console.WriteLine("\nDe la vie en moins !");
                if (vie <= 0) enVie = false;
            }
        }

        public double GetVie() { return this.vie; }
        public double GetAttaque() { return this.pointAttaque; }

    }
}
